import React from 'react';
import './YouTubeComp.css';

const YouTubeComp = (yy) => {
  return (
    <div className="youtube-wrapper">
      <div className="img-thumb">
        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Ragdoll_from_Gatil_Ragbelas.jpg/220px-Ragdoll_from_Gatil_Ragbelas.jpg" alt=""/>
        <p className="time">{yy.time}</p>
      </div>
      
      <p className="title">{yy.title}</p>
      <p className="desc">{yy.desc}</p>
    </div>
      
  )
}

// props di sini adalah bawaan react
YouTubeComp.defaultProps = {
  time:'00.00',
  title:'Title Here',
  desc:'xx ditonton, x hari yang lalu'
}
export default YouTubeComp;