// Libraries
import React, { Component, Fragment, createContext } from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';

//Pages
import Product from '../pages/Product/Product';
import LifeCycleComp from '../pages/LifeCycleComp/LifeCycleComp';
import BlogPost from '../pages/BlogPost/BlogPost';
import YoutubeCompPage from '../pages/YoutubeCompPage/YoutubeCompPage';
import DetailPost from '../pages/BlogPost/DetailPost/DetailPost';
import Hooks from '../pages/Hooks/Hooks';

// Style
import './Home.css';
import GlobalProvider from '../../context/context';



class Home extends Component {


  // componentDidMount() {
  //   setTimeout(()=> {
  //     this.setState({
  //       showComponent: false
  //     })
  //   }, 15000)
  // }

  render() {
    return(
      <Router>
        <Fragment>
          <div className="navigator">
            <Link to="/" >Blog Post</Link>
            <Link to="/product" >Product</Link>
            <Link to="/lifecycle" >Lifecycle</Link>
            <Link to="/youtube-component" >Youtube</Link>
            <Link to="/hooks" >Hooks</Link>
          </div>
          <Route path="/" exact component={BlogPost} />
          <Route path="/detail-post/:postID" component={DetailPost}/>
          <Route path="/product" component={Product} />
          <Route path="/lifecycle" component={LifeCycleComp} />
          <Route path="/youtube-component" component={YoutubeCompPage} />
          <Route path="/Hooks" component={Hooks} />
        </Fragment>
      </Router>
    )
  }
}

export default GlobalProvider(Home);