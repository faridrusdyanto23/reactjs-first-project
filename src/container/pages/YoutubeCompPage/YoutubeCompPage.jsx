import React, {Component, Fragment} from 'react';
import YouTubeComp from '../../../component/YouTubeComp/YouTubeComp';

class YoutubeCompPage extends Component {
    render() {
        return (
            <Fragment>
                <p>Youtube Component</p>
                <hr/>
                <YouTubeComp 
                time="7.12" 
                title="Tutorial ReactJs-Bagian 1"
                desc="2x ditonton, 2 hari yang lalu"
              />
              <YouTubeComp 
                time="8.02" 
                title="Tutorial ReactJs-Bagian 2"
                desc="3x ditonton, 9 hari yang lalu"
              />
              <YouTubeComp 
                time="5.04" 
                title="Tutorial ReactJs-Bagian 3"
                desc="4x ditonton, 7 hari yang lalu"
              />
              <YouTubeComp 
                time="4.12" 
                title="Tutorial ReactJs-Bagian 4"
                desc="20x ditonton, 5 hari yang lalu" />
              <YouTubeComp/>
            </Fragment>
        )
    }
}
export default YoutubeCompPage;