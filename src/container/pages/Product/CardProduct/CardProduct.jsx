import React, { Component } from 'react';
import Counter from './Counter';

class CardProduct extends Component {
  
  render() {
    return (
      <div className="card">
        <div className="img-thumb-prod">
          <img src="https://s3-ap-southeast-1.amazonaws.com/etanee-images/product/colonel_ori_pack10.jpg" alt="product_image"/>
        </div>
        <p className="prdouct-title">Daging Ayam Rasa Original [1 Carton-10 pack]</p>
        <p className="prdouct-price">Rp 410.000</p>
        <Counter />
      </div>
    );
  }
}

export default CardProduct;